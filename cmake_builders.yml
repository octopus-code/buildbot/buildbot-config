# CMake Builder Configurations
# --------------------------------

# Specifies all top-level fields, assigned to every builder.
# Defaults are overwritten by specific builders.
# In the case of dicts, only specified keys are overwritten.
defaults:
  preset:
  workers:
    - !WorkerGroup 'tentacles'               # Defined in workers.yml
  module:
    package_manager: PackageManager.SPACK    # Package manager
    version: 24a                             # Spack toolchain version to use
  env:                                       # Environment variables
    # Parallelization and load balancing is handled by ctest. Buildbot just needs to set the appropriate
    # CTEST_PARALLEL_LEVEL to the maximum number of processors available on the node.
    CTEST_PARALLEL_LEVEL: 16                 # TODO: This should be set depending on the builder
    CMAKE_BUILD_PARALLEL_LEVEL: 16
    MPIEXEC: 'orterun --map-by socket'
    OMP_NUM_THREADS: 1                       # TODO: This should be set depending on the builder
  test:
    timeout: 1800
    valgrind: False


cmake_foss_2021a_min_serial:                            # Unique builder name
    preset: 'foss-min'                                  # CMake preset in Octopus repository.
    module:
      toolchain: &foss-21a 'foss2021a'  # base module load `cmd`
      toolchain_variant: 'serial'                       # append to the above `toolchain` with `-` prepended

cmake_foss_2021a_min_mpi:
    preset: 'foss-min-mpi'
    module:
      toolchain: *foss-21a
      toolchain_variant: 'mpi'

cmake_foss_2021a_full_serial:
    preset: 'foss-full'
    module:
      toolchain: *foss-21a
      toolchain_variant: 'serial'

cmake_foss_2021a_full_mpi:
    preset: 'foss-full-mpi'
    module:
      toolchain: *foss-21a
      toolchain_variant: 'mpi'

cmake_foss_2021a_full_cuda_mpi:
    preset: 'foss-full-cuda-mpi'
    workers:
    - !WorkerGroup 'accelerated_tentacles'
    module:
      toolchain: *foss-21a
      toolchain_variant: 'cuda-mpi'

cmake_foss_2021a_full_cuda_mpi_omp:
    preset: 'foss-full-cuda-mpi-omp'
    workers:
    - !WorkerGroup 'accelerated_tentacles'
    module:
      toolchain: *foss-21a
      toolchain_variant: 'cuda-mpi'
    env:
      OMP_NUM_THREADS: 2

# foss-2022a

cmake_foss_2022a_min_serial:
    preset: 'foss-min'
    module:
      toolchain: &foss-22a 'foss2022a'
      toolchain_variant: 'serial'

cmake_foss_2022a_min_mpi:
    preset: 'foss-min-mpi'
    module:
      toolchain: *foss-22a
      toolchain_variant: 'mpi'

cmake_foss_2022a_full_serial:
    preset: 'foss-full'
    module:
      toolchain: *foss-22a
      toolchain_variant: 'serial'

cmake_foss_2022a_full_mpi:
    preset: 'foss-full-mpi'
    module:
      toolchain: *foss-22a
      toolchain_variant: 'mpi'

cmake_foss_2022a_full_openmp:
    preset: 'foss-full-openmp'
    module:
      toolchain: *foss-22a
      toolchain_variant: 'serial'

cmake_foss_2022a_full_openmp_mpi:
    preset: 'foss-full-openmp-mpi'
    module:
      toolchain: *foss-22a
      toolchain_variant: 'mpi'

cmake_foss_2022a_full_cuda_mpi:
    preset: 'foss-full-cuda-mpi'
    workers:
    - !WorkerGroup 'accelerated_tentacles'
    module:
      toolchain: *foss-22a
      toolchain_variant: 'cuda-mpi'

cmake_foss_2022a_full_cuda_mpi_omp:
    preset: 'foss-full-cuda-mpi-omp'
    workers:
    - !WorkerGroup 'accelerated_tentacles'
    module:
      toolchain: *foss-22a
      toolchain_variant: 'cuda-mpi'
    env:
      OMP_NUM_THREADS: 2

# foss-2022b

cmake_foss_2022b_min_serial:
    preset: 'foss-min'
    module:
      toolchain: &foss-22b 'foss2022b'
      toolchain_variant: 'serial'

cmake_foss_2022b_min_mpi:
    preset: 'foss-min-mpi'
    module:
      toolchain: *foss-22b
      toolchain_variant: 'mpi'

cmake_foss_2022b_full_serial:
    preset: 'foss-full'
    module:
      toolchain: *foss-22b
      toolchain_variant: 'serial'

cmake_foss_2022b_full_mpi:
    preset: 'foss-full-mpi'
    module:
      toolchain: *foss-22b
      toolchain_variant: 'mpi'

cmake_foss_2022b_full_cuda_mpi:
    preset: 'foss-full-cuda-mpi'
    workers:
    - !WorkerGroup 'accelerated_tentacles'
    module:
      toolchain: *foss-22b
      toolchain_variant: 'cuda-mpi'

cmake_foss_2022b_full_cuda_mpi_omp:
    preset: 'foss-full-cuda-mpi-omp'
    workers:
    - !WorkerGroup 'accelerated_tentacles'
    module:
      toolchain: *foss-22b
      toolchain_variant: 'cuda-mpi'
    env:
      OMP_NUM_THREADS: 2


# intel-2022b

cmake_intel_2022b_min_serial:
    preset: 'intel-min'
    module:
      toolchain: &intel-22b 'intel2022b'
      toolchain_variant: 'serial'

cmake_intel_2022b_min_impi:
    preset: 'intel-min-mpi'
    module:
      toolchain: *intel-22b
      toolchain_variant: 'mpi'

cmake_intel_2022b_full_serial:
    preset: 'intel-full'
    module:
      toolchain: *intel-22b
      toolchain_variant: 'serial'

cmake_intel_2022b_full_impi:
    preset: 'intel-full-mpi'
    module:
      toolchain: *intel-22b
      toolchain_variant: 'mpi'

# foss-2023a

cmake_foss_2023a_min_serial:
    preset: 'foss-min'
    module:
      toolchain: &foss-23a 'foss2023a'
      toolchain_variant: 'serial'

cmake_foss_2023a_min_mpi:
    preset: 'foss-min-mpi'
    module:
      toolchain: *foss-23a
      toolchain_variant: 'mpi'

cmake_foss_2023a_full_serial:
    preset: 'foss-full'
    module:
      toolchain: *foss-23a
      toolchain_variant: 'serial'

cmake_foss_2023a_full_mpi:
    preset: 'foss-full-mpi'
    module:
      toolchain: *foss-23a
      toolchain_variant: 'mpi'

cmake_foss_2023a_full_omp:
    preset: 'foss-full-omp'
    module:
      toolchain: *foss-23a
      toolchain_variant: 'serial'

cmake_foss_2023a_full_mpi_omp:
    preset: 'foss-full-mpi-omp'
    module:
      toolchain: *foss-23a
      toolchain_variant: 'mpi'

cmake_foss_2023a_full_cuda_mpi:
    preset: 'foss-full-cuda-mpi'
    workers:
    - !WorkerGroup 'accelerated_tentacles'
    module:
      toolchain: *foss-23a
      toolchain_variant: 'cuda-mpi'

cmake_foss_2023a_full_cuda_mpi_omp:
    preset: 'foss-full-cuda-mpi-omp'
    workers:
    - !WorkerGroup 'accelerated_tentacles'
    module:
      toolchain: *foss-23a
      toolchain_variant: 'cuda-mpi'
    env:
      OMP_NUM_THREADS: 2


# intel-2023a

cmake_intel_2023a_min_serial:
    preset: 'intel-min'
    module:
      toolchain: &intel-23a 'intel2023a'
      toolchain_variant: 'serial'

cmake_intel_2023a_min_mpi:
    preset: 'intel-min-mpi'
    module:
      toolchain: *intel-23a
      toolchain_variant: 'mpi'

cmake_intel_2023a_full_serial:
    preset: 'intel-full'
    module:
      toolchain: *intel-23a
      toolchain_variant: 'serial'

cmake_intel_2023a_full_mpi:
    preset: 'intel-full-mpi'
    module:
      toolchain: *intel-23a
      toolchain_variant: 'mpi'


cmake_eb_foss_2022b_min:
    preset: 'foss-min'
    module:
      version: &eb-current-version 2022b
      package_manager: PackageManager.EB
      toolchain: foss
      toolchain_variant: 'full'

cmake_eb_intel_2022b_min:
    preset: 'intel-min'
    module:
      version: *eb-current-version
      package_manager: PackageManager.EB
      toolchain: intel
      toolchain_variant: 'full'
