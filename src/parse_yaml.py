"""Yaml file parsing.
"""
from abc import ABC, abstractmethod
import os
from pathlib import Path
from typing import Dict, Union, Optional, Callable, Type, List
import yaml


# YAML Loader types do not inherit from a common base class
# hence define a generic loader type, for type-hinting
loader_type = Union[Type[yaml.BaseLoader],
                    Type[yaml.FullLoader],
                    Type[yaml.SafeLoader],
                    Type[yaml.Loader],
                    Type[yaml.UnsafeLoader]]


class AbstractConstructor(ABC):
    """ Abstract base class for custom YAML tag/constructor pair.

    Allows private data to be shared with the constructor method,
    of which will be assigned to a YAML loader.

    Usage:
    ```
    # Define child class, with specific behaviour
    class CustomConstructor(AbstractConstructor):
        tag = '!MyTag'

        def __init__(self, data):
            self.data = data
            ...
        def constructor(loader, node):
            ...

    # Initialise instance with some data, and add its (tag, constructor) to a YAML loader.
    custom = CustomConstructor(data)
    yaml.SafeLoader.add_constructor(custom.tag, custom.constructor)
    ```
    """
    tag: str

    @abstractmethod
    def constructor(self, loader: loader_type, node: yaml.Node):
        ...


def loader_factory(custom_constructors: Union[AbstractConstructor, List[AbstractConstructor]],
                   yaml_loader=yaml.SafeLoader) -> loader_type:
    """ YAML loader factory.

    Given one or more custom tag-constructor instances, return a custom YAML loader.

    :param custom_constructors: Custom tag-constructor instance/s.
    :param yaml_loader: Optional base YAML loader.
    :return: custom_loader: Custom YAML loader.
    """
    # Initialise custom loader with a YAML loader
    custom_loader = yaml_loader

    if not isinstance(custom_constructors, list):
        custom_constructors = [custom_constructors]

    # Assign input custom tag-constructors to the base YAML loader
    for custom in custom_constructors:
        custom_loader.add_constructor(custom.tag, custom.constructor)
    return custom_loader


def robust_yaml_arg(load_func: Callable) -> Callable:
    """Decorator for YAML loader to handle file or YAML-formatted string as an argument.
    Note, there may be cleaner ways to implement this.

    :param load_func: Yaml loader.
    :return modified_function: Modified Yaml loader.
    """
    def modified_function(file_or_yaml_str: Union[str, Path], custom_constructor: Optional[dict] = None):

        # 1. Must be a file
        if isinstance(file_or_yaml_str, Path):

            if not file_or_yaml_str.is_file():
                raise FileNotFoundError(f'File not found: {file_or_yaml_str}')

            with open(file_or_yaml_str, "r") as fid:
                yaml_str = fid.read()
            return load_func(yaml_str, custom_constructor)

        # 2. Can be a file name or yaml string
        elif isinstance(file_or_yaml_str, str):

            # File
            if os.path.isfile(file_or_yaml_str):
                with open(file_or_yaml_str, "r") as fid:
                    yaml_str = fid.read()
                return load_func(yaml_str, custom_constructor)

            # `Assume` string contains yaml and pass on
            return load_func(file_or_yaml_str, custom_constructor)

        # Argument is wrong type
        else:
            raise ValueError(f'First argument is not a file or string: {file_or_yaml_str}')

    return modified_function


# Signature of custom_constructor dict
CustomConType = Dict[str, Callable]


@robust_yaml_arg
def yaml_load(yaml_str, custom_constructor: Optional[CustomConType] = None) -> dict:
    """ Parse YAML file.

    Expect custom_constructor of the form:
    {u'!Group': enum_group_constructor}

    :param yaml_str: File name or yaml-formatted string
    :param custom_constructor: Optional custom constructors applied to parsed data.
    :return config: Dict of parsed data.
    """
    # Extend loader to parse the data through custom constructors
    if custom_constructor is None:
        custom_constructor = {}

    custom_loader = yaml.SafeLoader
    for tag, constructor in custom_constructor.items():
        custom_loader.add_constructor(tag, constructor)

    # Parse yaml
    try:
        config = yaml.load(yaml_str, Loader=custom_loader)
    except yaml.YAMLError:
        raise yaml.YAMLError(f'Invalid formatting in YAML file: {yaml_str}')

    return config
