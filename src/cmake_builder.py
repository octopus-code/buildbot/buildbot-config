"""CMake Builder class and assistant functions.
"""
from __future__ import annotations

import copy
from pathlib import Path
from typing import TYPE_CHECKING

from buildbot.plugins import util, steps
from buildbot.config import BuilderConfig

from src.custom_steps import GitLabStep, BBGitStep, OctopusTestUploadStep
from src.yaml_spec import PackageManager, parse_builders_yaml

if TYPE_CHECKING:
    from collections.abc import Callable
    from buildbot.process.buildstep import BuildStep


def spack_module_command(toolchain: str, version: str, toolchain_variant: str = "") -> str:
    f"""
    Define Spack module command, for use with CMake.

    Notes
    -----------
    * The toolchain name is constructed as `f"{toolchain}"` if not `toolchain_variant` is passed,
      otherwise `f"{toolchain}-{toolchain_variant}"`
    
    * Required as `mpsd-modules 23b` is not loadable from the buildbot shell.

    * For CMake we need to unset CPATH and LIBRARY_PATH. Otherwise, pkg-config sets CFLAGS
      that miss fortran include paths. This is valid for autotools, which uses CPATH and LIBRARY_PATH
      to determine them, but not CMake.
      
    .. seeAlso:: :py:func:`module_command`

    :param toolchain: Module toolchain base name to load, i.e. `toolchains/foss2021a`
    :param version: Spack environment version to use (e.g. '23b', aka mpsd_release)
    :param toolchain_variant: Module toolchain to load, i.e. `serial`
    :return: modules_command: Full module command, with necessary export flags
    """

    old_releases = ['dev-23a','23b']

    if toolchain_variant:
        toolchain += f"-{toolchain_variant}"

    module_cmd = '/usr/share/lmod/lmod/libexec/lmod'
    modules_command = f"eval `{module_cmd} sh use /opt_mpsd/$MPSD_OS/{version}/$MPSD_MICROARCH/lmod/Core` && "
    # TODO: Remove hard-coded ninja when toolchain includes it
    if version in old_releases:
        modules_command += f"eval `{module_cmd} sh load toolchains/{toolchain} ninja` && "
    else:
        modules_command = modules_command + f"eval `{module_cmd} sh load toolchain/{toolchain}` && "
        modules_command = modules_command + f"eval `{module_cmd} sh load octopus-dependencies/full` && "

    modules_command += "unset CPATH && unset LIBRARY_PATH"
    return modules_command


def eb_module_command(toolchain: str, version: str, toolchain_variant: str) -> str:
    """
    Define EasyBuild module command, for use with CMake.

    .. seeAlso:: :py:func:`module_command`

    :param toolchain: Module toolchain base name to load, i.e. `toolchains/foss2021a`
    :param version: Spack environment version to use
    :param toolchain_variant: Module toolchain to load, i.e. `serial`
    :return: modules_command: Full module command, with necessary export flags
    """

    module_cmd = "/usr/share/lmod/lmod/libexec/lmod"
    modules_command = f"eval `{module_cmd} sh use $EASYBUILD_PREFIX/{version}/modules/Core` && "
    # TODO: Remove hardcoded CMake and Ninja loading
    modules_command += f"eval `{module_cmd} sh load {toolchain} CMake Ninja Octopus/{toolchain_variant}` && "
    modules_command += "unset CPATH && unset LIBRARY_PATH"
    return modules_command


def module_command(package_manager: PackageManager) -> Callable[[str, str, str], str]:
    """
    Choose which module command function to use


    .. seeAlso:: :py:func:`spack_module_command`, :py:func:`eb_module_command`

    :param package_manager: Package manager
    :return cmd_func: Function defining the module command
    """
    cmd_func = {
        PackageManager.EB: eb_module_command,
        PackageManager.SPACK: spack_module_command
    }
    try:
        return cmd_func[package_manager]
    except KeyError:
        raise KeyError(f'Package manager not recognised: {package_manager.name}\n'
                       f'Valid choices are {PackageManager.names()}.')


class CmakeBuilderConfig:
    """ Class for parsing and processing the cmakebuilders.yml config file.

    Example Usage
    --------------
    workers = yaml_load("workers.yml")
    cmake_config = CmakeBuilderConfig(workers, file_name="cmake_builders.yaml")
    cmake_builders: dict = cmake_config.builders
    for name, settings in cmake_builders.items():
        c['builders'].append(octopus_cmake_builder(name, **settings)
    """
    default_file_name = Path("cmake_builders.yml")

    def __init__(self, workers: dict, file_name: str | Path | None = None):
        """ Parse YAML builders configuration file.

        :param workers: Dict of valid workers
        :param file_name: YAML builders file name.
        :param perform_checks: Perform checks on flags and builders.
        """
        self.file_name = self.default_file_name if file_name is None else Path(file_name)
        self._config = self._parse_yaml(workers)
        self.defaults = self.preprocess_defaults()
        # Ensure values are strings for env dict
        self.defaults['env'] = {key: str(value) for key, value in self.defaults['env'].items()}
        self.builders = self.set_builders()

    def preprocess_defaults(self) -> dict:
        """ Preprocess Default fields.

        :return: defaults: Defaults with specific fields processed
        """
        defaults = self._config.pop('defaults')

        flatten = lambda l: sum(map(flatten, l), []) if isinstance(l, list) else [l]
        defaults['workers'] = flatten(defaults['workers'])

        # TODO(Alex) Issue 47. Move to YAML custom constructor
        pm_str = defaults['module']['package_manager'].split('.')[-1]
        defaults['module']['package_manager'] = PackageManager[pm_str]

        return defaults

    def _parse_yaml(self, workers: dict) -> dict:
        """ Parse YAML builders file.

        :param workers: Dict of valid workers
        :return: config: File contents.
        """
        return parse_builders_yaml(self.file_name, workers)

    def set_builders(self) -> dict:
        """ Set builders dictionary

        :return: builders: Dict of builders
        """
        # Recursively flatten nested list
        flatten = lambda l: sum(map(flatten, l), []) if isinstance(l, list) else [l]

        builders = {}
        for name, settings in self._config.items():
            # Initialise
            builders[name] = copy.deepcopy(self.defaults)

            # TODO(Alex) Issue 47. Move to YAML custom constructor
            # Ensure values are strings for env sub-dict
            if settings.get('env', False):
                settings['env'] = {key: str(value) for key, value in settings['env'].items()}

            # Flatten list (assume it's possible in WorkerConstructor, but no idea how)
            if settings.get('workers', False):
                settings['workers'] = flatten(settings['workers'])

            # TODO(Alex) Issue 47. Move to YAML custom constructor
            if settings.get('module', {}).get('package_manager', False):
                pm_str = settings['module']['package_manager'].split('.')[-1]
                settings['module']['package_manager'] = PackageManager[pm_str]

            # Fill with user-defined values
            for key, values in settings.items():
                # Replace defaults only when defined in settings
                if isinstance(values, dict):
                    builders[name][key].update(**values)
                # Always replace when defined in settings
                else:
                    builders[name][key] = values

        return builders


def run_tests(step: BuildStep) -> bool:
    """ Determine whether to run tests.

    :param step: Buildbot build step
    :return: Whether to run tests
    """
    options = step.build.getProperty("options", default=None)
    if not options:
        # Not in the ForcedScheduler run, return the default (True, i.e. not skip_tests)
        return True
    # Otherwise use the value from ForcedScheduler. See default value from there
    return not options["skip_tests"]


def octopus_cmake_builder(build_name: str,
                          preset: str,
                          workers: str | list[str],
                          module: dict,
                          env: dict,
                          # TODO: Migrate test variants to cmake builder
                          test: dict,
                          timeout: int = 1800,
                          ) -> BuilderConfig:
    """Set up an Octopus build for CMake, hard-coded for Spack module base.

    :param build_name: Unique builder name.
    :param preset: Cmake preset string.
    :param workers: Workers to run the pipeline on.
    :param module: Dict of module steps/commands.
    :param env: Environment variables.
    :param test: Test variables.
    :param timeout: Optional. Builder testing time-out in seconds.

    :return: A CMake-based builder configuration instance.
    """
    try:
        package_manager = module.pop('package_manager')
        module_cmd_factory = module_command(package_manager)
    except KeyError as err:
        raise KeyError("module.package_manager must be specified for a CMake builder") from err

    my_env = env
    # Add other dynamically defined env variables
    my_env["CHECK_REPORT"] = util.Interpolate("%(prop:buildername)s_%(src::revision)s")

    cmake_workers = workers if isinstance(workers, list) else [workers]
    module_cmd = module_cmd_factory(**module)

    # Define build factory
    build_factory = util.BuildFactory()
    build_factory.addStep(GitLabStep)
    build_factory.addStep(BBGitStep)

    build_factory.addStep(steps.ShellCommand(description="Update submodules",
                                             descriptionDone="Submodules Updated",
                                             command=f"git submodule update --init --recursive",
                                             haltOnFailure=True))

    # Build directory is standardized as follows. Should not need to use, but upstream does not support globbing
    # https://github.com/buildbot/buildbot/issues/3874
    build_dir = f"cmake-build-ci-{preset}"

    # Proper cmake support is lacking:
    # https://github.com/buildbot/buildbot/issues/7158
    build_factory.addStep(steps.Configure(description="Running CMake configure",
                                          name='cmake configure',
                                          descriptionDone="CMake configure",
                                          command=module_cmd + f" && cmake  --preset {preset} --fresh",
                                          logfiles={
                                              "config.h": f"{build_dir}/src/include/config.h",
                                              "Octopus.info": f"{build_dir}/Octopus.info",
                                              "CMakeCache.txt": f"{build_dir}/CMakeCache.txt"
                                          },
                                          ))

    build_factory.addStep(steps.Compile(description="Running CMake Build",
                                        name='cmake build',
                                        descriptionDone="CMake Build",
                                        command=module_cmd + f" && cmake --build --preset {preset}",
                                        ))
    
    build_factory.addStep(steps.ShellCommand(description='ldd',
                                     name='run ldd',
                                     descriptionDone='ldd done',
                                     command=module_cmd + f" && ldd {build_dir}/octopus | sort > ldd.out",
                                     logfiles={"ldd.out": "ldd.out"},
                                     haltOnFailure=False))


    # Current implementation does not add any metadata
    # Need to add a CTest subclass and invoke setTestResults within evaluateCommand
    build_factory.addStep(steps.Test(description="Running CTest",
                                     name='ctest',
                                     descriptionDone="CTest",
                                     command=module_cmd + f" && ctest --preset {preset}",
                                     logfiles={
                                         "results.xml": f"{build_dir}/results.xml"
                                     },
                                     doStepIf=run_tests,
                                     ))
    build_factory.addStep(OctopusTestUploadStep(timeout=timeout,
                                                build_dir=build_dir,
                                                doStepIf=run_tests
                                                ))

    return util.BuilderConfig(name=build_name, workernames=cmake_workers, factory=build_factory, env=my_env)
