"""URL Definitions
"""
from abc import ABC, abstractmethod
import socket

class URLDefinitions(ABC):
    """Abstract base class for URL containers.
    """
    @abstractmethod
    def title(self):
        ...

    @abstractmethod
    def buildbot(self):
        ...

    @abstractmethod
    def octopus_git(self):
        ...

    @abstractmethod
    def website_git(self):
        ...

    @abstractmethod
    def bbscripts_git(self):
        ...

    @abstractmethod
    def webrootfolder(self):
        ...


class BaseDefinitions(URLDefinitions):

    # check for 'staging' in the hostname and set the _web url accordingly
    if "staging" in socket.getfqdn():
        _web = "http://mpsd-octopus-code-staging.desy.de"
    else:
        _web = "https://www.octopus-code.org"

    _gitlab = "git@gitlab.com:octopus-code"
    _webroot = "/mpsd/theory/octopus-code-org/www/"

    # Note, could use @property on these methods
    def title(self):
        return self._web

    def buildbot(self):
        return self._web + "/buildbot/"

    def octopus_git(self):
        return self._gitlab + "/octopus.git"

    def website_git(self):
        return self._gitlab + "/octopus-documentation.git"

    def bbscripts_git(self):
        return self._gitlab + "/buildbot/buildbot-scripts.git"

    def webrootfolder(self):
        return self._webroot


def url_factory() -> URLDefinitions:
    """ Return an instance of URLDefinitions.

    This takes no arguments while only one class of
    URLDefinitions exists.

    :return:  URL definitions instance.
    """
    return BaseDefinitions()
