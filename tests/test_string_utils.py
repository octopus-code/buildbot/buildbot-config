import pytest

from src.string_utils import substitute_string


def test_substitute_string():
    matches = ['<WEATHER>', '<DAY>']
    replacements = ['rainy', 'today']
    string = 'It is a <WEATHER> day <DAY>'
    new_string = substitute_string(matches, replacements, string)
    assert new_string == 'It is a rainy day today'


def test_substitute_string_duplicates():
    """Behaviour for replacement for multiple, identical matches.
    """
    matches = ['ROOT']
    replacements = ['string']
    string = 'This contains ROOT and then a repeat, ROOT'

    new_string = substitute_string(matches, replacements, string)
    assert new_string == 'This contains string and then a repeat, string', \
        "Expect all instances of match to be replaced"


def test_substitute_string_null_behaviour():
    """When matches are not in the string, or matches is empty.
    """
    matches = ['ROOT']
    replacements = ['string']
    string = 'No matches in this string'
    new_string = substitute_string(matches, replacements, string)
    assert new_string == string, "No matches in string, hence no substitutions"

    matches = []
    replacements = []
    string = 'Some arbitrary string'
    new_string = substitute_string(matches, replacements, string)
    assert new_string == string, "matches is empty, hence no substitutions"


def test_substitute_string_input_error():
    matches = ['ROOT', 'LIB']
    replacements = ['/users/home']
    string = 'ROOT/LIB'

    with pytest.raises(ValueError) as error:
        new_string = substitute_string(matches, replacements, string)
    assert error.value.args[0] == 'Each match should have a replacement', \
        "`replacements` should have the same number of entries as `matches`"
