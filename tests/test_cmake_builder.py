import pytest

from src.cmake_builder import CmakeBuilderConfig, octopus_cmake_builder
from src.yaml_spec import PackageManager


@pytest.fixture()
def cmake_builders_yaml() -> str:
    yaml_str = """
defaults:
  preset:
  workers:
    - !WorkerGroup 'tentacles'               # Defined in workers.yml
  module:
    package_manager: PackageManager.SPACK    # Package manager
  env:                                       # Environment variables
    OCT_TEST_NJOBS: 16
  test:
    timeout: 1800
    valgrind: False

foss_2021a_min_serial:                         # Unique builder name
    preset: 'foss_min'                         # CMake preset in Octopus repository.
    module:
      pre-load: 'mpsd-modules 23b'             # Any commands required to run before loading a meta-module
      load: 'toolchains/foss2021a-serial'      # module load `cmd`
    env:
      OCT_TEST_NJOBS: 8

    """
    return yaml_str


def test_cmake_builder_config(cmake_builders_yaml: str, tmp_path):
    file = tmp_path / "cmake_builders.yml"
    file.write_text(cmake_builders_yaml)
    workers = {'tentacles': ['tentacle1', 'tentacle2', 'tentacle3']}

    expected_builder = {'preset': 'foss_min',
                        'workers': ['tentacle1', 'tentacle2', 'tentacle3'],
                        'module': {'package_manager': PackageManager.SPACK,
                                   'pre-load': 'mpsd-modules 23b',
                                   'load': 'toolchains/foss2021a-serial'},
                        'env': {'OCT_TEST_NJOBS': '8'},
                        'test': {'timeout': 1800,
                                 'valgrind': False
                                 }
                        }

    config = CmakeBuilderConfig(workers, file_name=file)

    assert list(config.builders.keys()) == ['foss_2021a_min_serial']
    assert config.builders['foss_2021a_min_serial'] == expected_builder

