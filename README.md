# Octopus Buildbot README

TODO. Add some details regarding this build bot implementation.

## Defining Builders and Flags in `builders.yaml`

### Builder Specifications

A builder is defined in yaml as:

```yaml
PM:
  toolchain:
    version_options:
```

where:

 * The first key corresponds to the package manager `['EB', 'SPACK']`
 * The second key corresponds to the `toolchain`
 * The third key corresponds to the `version_options`

A specific example of this would be:

```yaml
EB:
  foss:
    2020b_mpi:
```

where `version_options = 2020b_mpi`. 

* `version` is inferred from the prefix in `version_options`. In this case, the prefix is `2020b`.
* Note, the symbol `'_'` must be used as the prefix delimiter.
* `version_options` is used instead of `version`, in order to provide a unique key 
for the corresponding dictionary, upon parsing of the file, however none of the information following the prefix is
utilised by code.


**Explicitly Defining Options and Variants**

* The buildname is defined as `toolchain + '-' + version_options`
* **NO** options or variants are inferred from the `buildname` or `version_options`. These must be explicitly specified.
* For a list of optional arguments, and their defaults, please see the `optional_builder_defaults` field in
  [builders.yaml](builders.yaml).

An example builder, specified with mandatory fields:

```yaml
EB:
  foss:
    2020b:
      workers: 'tentacles'
      var:
        CFLAGS: '{foss.cflags}'
        CXXFLAGS: '{foss.cxxflags}'
        FCFLAGS: '{foss.fflags}'
        LOCAL_MAKEFLAGS: '-j 16'
        OCT_TEST_NJOBS: 16
      flags: '{foss.configure_flags_serial}' 
```

No optional keys are specified, causing missing fields to utilise the defaults defined in [builders.yaml](builders.yaml).


**An Alternative Approach (which we do not use)**

One could have alternatively implemented key fields as:

```yaml
EB:
  buildername:
    version: 
    workers:
    var:
    ...
```

such that:

* The `buildername` is unique
* The nesting is reduced
* `version` is explicitly specified, and does not require any processing within the code

This would require some reworking of the routines in the [BuilderConfig](src/builder.py) class, and would benefit
from standardising the builder names (`['fosscuda', 'intelcuda']` being the outliers). See the discussion in [MR 58](https://gitlab.com/octopus-code/buildbot/buildbot-config/-/merge_requests/58/)
for more details. 


### Flags

Flag variables are defined like `{pm.flag_name}`, where `'.'` acts  as a delimiter, denoting nesting. Variables are processed in python, such
that `{pm.flag_name}` is replaced by an explicit definition.

```yaml
 pm:
    flag_name: '-Wall -O3 -march=native -funroll-loops'         # Definition
 toochain:
    another_flag_name: '{pm.flag_name}'                         # Variable
```

Bash variables of the form `${VAR}` are ignored by the regex, however double braces `{{VAR}}` will erroneously get matched. 
If this is required, please update the regex implementation in the [YamlFormat](src/yaml_spec.py) class.
